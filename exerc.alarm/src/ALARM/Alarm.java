package ALARM;

import java.util.ArrayList;
import java.util.List;

public class Alarm {
	
	private List<AlarmListener> listeners = new ArrayList<AlarmListener>();
	private String pin="1234";
	private Thread life = new Thread();
	
	Alarm()
	{
		life.start();
	}
	
	void addListener(AlarmListener listener)
	{
		listeners.add(listener);
	}
	void removeListener(AlarmListener listener)
	{
		listeners.remove(listener);
	}
	void enterPin(String pin)
	{
		if(this.pin.equals(pin))
		{
			correctEnteredPin();
		}
		else
			wrongEnteredPin();
	}
	void wrongEnteredPin()
	{
		EnteredPinEvent event = new EnteredPinEvent();
		
		for(AlarmListener x : listeners)
		x.alarmTurnedOn(event);
		
	}
	void correctEnteredPin()
	{
		EnteredPinEvent event = new EnteredPinEvent();
		
		for(AlarmListener x : listeners)
		x.alarmTurnedOff(event);
	}
	public void run()
	{
		while(true)
		{
			int r = (int)(Math.random()*2);
			switch(r)
			{
			case 0: correctEnteredPin();break;
			case 1: wrongEnteredPin();break;
			}
			try{
				sleep(10000);
			} catch (InterruptedException e) {
					e.printStackTrace();
			}
		}
	}

}
