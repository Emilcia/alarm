package ALARM;

public class Main {

	public static void main(String[] args) 
	{
		Alarm a = new Alarm();
		SoundAlert alert = new SoundAlert();
		Police police = new Police();
		
		a.addListener(alert);
		a.addListener(police);
		a.enterPin("1234");
	}

}
