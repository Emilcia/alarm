package ALARM;

import java.text.SimpleDateFormat;

public class SoundAlert implements AlarmListener {

	private SimpleDateFormat format = new SimpleDateFormat("dd - MM - yyyy  hh:mm");

	public void alarmTurnedOn(EnteredPinEvent event) 
	{
		System.out.println("PIPIPIPI "+format.format(event.getEventDate()));
	}

	public void alarmTurnedOff(EnteredPinEvent event) 
	{
		System.out.println("PIPIPIPI turned off "+format.format(event.getEventDate()));		
	}

}
