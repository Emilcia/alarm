package ALARM;

import java.text.SimpleDateFormat;

public class Police implements AlarmListener {

	private SimpleDateFormat format = new SimpleDateFormat("dd - MM - yyyy  hh:mm");

	public void alarmTurnedOn(EnteredPinEvent event) 
	{
		System.out.println("Police is comming "+format.format(event.getEventDate()));
	}

	public void alarmTurnedOff(EnteredPinEvent event) 
	{
		System.out.println("Police off "+format.format(event.getEventDate()));		
	}

}
