package ALARM;

import java.util.Date;

public class EnteredPinEvent {

	private Alarm alarm;
	private Date eventDate = new Date();
	
	public Alarm getAlarm()
	 {
		return alarm;
	 }
	public void setAlarm(Alarm alarm) 
	{
		this.alarm = alarm;
	}
	public Date getEventDate() 
	{
		return eventDate;
	}
	public void setEventDate(Date eventDate) 
	{
		this.eventDate = eventDate;
	}
	
	
	
	
}
